﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationDelay : MonoBehaviour {

	public float m_AnimationSpeed = 1.0f;

	// Use this for initialization
	void Start () {
		GetComponent<Animator>().speed = m_AnimationSpeed;
	}
}

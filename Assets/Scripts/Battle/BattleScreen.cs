﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleScreen : MonoBehaviour {

	/// <summary>
	/// reference to the battle screen for other classes to use
	/// similar to a singleton
	/// </summary>
	public static BattleScreen m_BattleScreen;

	/// <summary>
	/// list of cloud spawner's
	/// so we can delete all the clouds when the new screen opens
	/// </summary>
	private CloudMover[] m_CloudSpawners;

	struct SUnit {
		public GameObject m_UiGm;
		public GameObject m_UiPanelGm;
		public Unit m_Unit;
	}

	/// <summary>
	/// reference to the two unit images
	/// </summary>
	private SUnit[] m_Units = new SUnit[2];

	[HideInInspector]
	public bool m_IsOpen;

	//temp
	private float m_OpenTime;

	public bool m_OpenOnStart = false;

	enum battleStages {
		Start,
		LeftAttack,
		RightAttack,
		End,
	};

	/// <summary>
	/// times for animations to complete
	/// </summary>
	private float[] m_StageTimes = { 6, 1, 1, 4 };

	private battleStages m_CurrentStage;

	// Use this for initialization
	void Start () {
		m_BattleScreen = this;

		gameObject.SetActive(false);

		m_CloudSpawners = GetComponentsInChildren<CloudMover>(true);

		//hard coded way to find units
		Transform gmLeft = transform.GetChild(0).GetChild(0).GetChild(0);
		Transform gmRight = transform.GetChild(0).GetChild(0).GetChild(1);

		m_Units[0].m_UiGm = gmLeft.GetChild(3).gameObject;
		m_Units[0].m_UiPanelGm = gmLeft.GetChild(4).gameObject;

		m_Units[1].m_UiGm = gmRight.GetChild(3).gameObject;
		m_Units[1].m_UiPanelGm = gmRight.GetChild(4).gameObject;

		if (m_OpenOnStart) {
			open();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (m_IsOpen) {

			//what should be done every frame for each stage
			switch (m_CurrentStage) {
				case battleStages.Start:
					//nothing?
					break;
				case battleStages.LeftAttack:
					//nothing?
					break;
				case battleStages.RightAttack:
					//nothing?
					break;
				case battleStages.End:
					//nothing?
					break;
			}

			//if the time for this stage is up?
			if (Time.time > m_OpenTime + m_StageTimes[(int)m_CurrentStage]) {

				//what should be done at the end of these stages
				switch (m_CurrentStage) {
					case battleStages.Start:
						//nothing?
						break;
					case battleStages.LeftAttack:
						//nothing?
						break;
					case battleStages.RightAttack:
						//nothing?
						break;
					case battleStages.End:
						m_IsOpen = false;
						gameObject.SetActive(false);
						break;
				}

				//go to next stage
				nextStage();

				//what should be done at the start of these stages
				switch (m_CurrentStage) {
					case battleStages.Start:
						//imposable ??
						break;
					case battleStages.LeftAttack:
						Attack(m_Units[0], m_Units[1]);
						transform.GetChild(0).GetChild(3).GetComponent<Text>().text = "Left Attack";
						break;
					case battleStages.RightAttack:
						Attack(m_Units[1], m_Units[0]);
						transform.GetChild(0).GetChild(3).GetComponent<Text>().text = "Right Attack";
						break;
					case battleStages.End:
						transform.GetChild(0).GetChild(3).GetComponent<Text>().text = "End";
						break;
				}

			}
		}
	}

	//called when you want to open the battleScreen
	public void open(Unit a_LeftSide = null, Unit a_RightSide = null) {
		//set things up to start the battleScreen
		m_IsOpen = true;
		m_OpenTime = Time.time;
		gameObject.SetActive(true);
		m_CurrentStage = battleStages.Start;

		//set up unit information
		if (a_LeftSide != null) {
			m_Units[0].m_UiGm.GetComponent<Image>().sprite = a_LeftSide.GetComponent<SpriteRenderer>().sprite;
			m_Units[0].m_Unit = a_LeftSide;
		}
		if (a_RightSide != null) {
			m_Units[1].m_UiGm.GetComponent<Image>().sprite = a_RightSide.GetComponent<SpriteRenderer>().sprite;
			m_Units[1].m_Unit = a_RightSide;
		}
		updateUnitText(m_Units[0]);
		updateUnitText(m_Units[1]);


		//tell the clouds to their suff
		for (int i = 0; i < m_CloudSpawners.Length; i++) {
			m_CloudSpawners[i].start();
		}

		transform.GetChild(0).GetChild(3).GetComponent<Text>().text = "Temp Battle Start!";
	}

	private void nextStage() {
		m_CurrentStage++;
		m_OpenTime = Time.time;
	}

	private void Attack(SUnit a_Attack, SUnit a_Receiver) {
		//print("Attack " + a_Attack.m_Unit.transform.position);

		//if either the attack or receiver is dead then skip this stage
		if(a_Attack.m_Unit.m_Health <= 0 || a_Receiver.m_Unit.m_Health < 0) {
			//nextStage();
			return;
		}

		a_Receiver.m_Unit.attack(a_Attack.m_Unit.m_AttackStrength);

		updateUnitText(a_Receiver);
	}

	private void updateUnitText(SUnit a_Unit) {
		a_Unit.m_UiPanelGm.transform.GetChild(1).GetChild(1).GetComponent<Text>().text = a_Unit.m_Unit.m_Health + "/" + a_Unit.m_Unit.m_MaxHealth;
	}
}

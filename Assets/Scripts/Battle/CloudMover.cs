﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloudMover : MonoBehaviour {

	public RuntimeAnimatorController m_Animator;

	public Sprite[] m_Clouds;

	[Tooltip("Higher is less likely")]
	public int m_ChanceOfCloudSpawn = 100;

	// Use this for initialization
	void Start () {
		//m_StartingPos = transform.position;
		//m_Animator = GetComponent<Animator>();
		//m_Animator.StartPlayback();
		//m_Animator.Play("Base Layer");
	}
	
	// Update is called once per frame
	void Update () {
		//TODO this if slightly inefficient due to getting component each frame, make it stop
		for (int i = 0; i < transform.childCount; i++) {
			if (transform.GetChild(i).GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 1) {
				//m_Animator.Play("New Animation", 0, 0);
				Destroy(transform.GetChild(i).gameObject);
				//m_Animator.StartPlayback();
			}
		}

		if(Random.Range(0,m_ChanceOfCloudSpawn) == 0) {
			spawnCloud(0);
		}
	}

	public void start() {

		for (int i = 0; i< transform.childCount; i++) {
			Destroy(transform.GetChild(i).gameObject);
		}

		//spawn a cloud to start off the battle screen
		for(int i = 0; i < 2; i++) {
			if (Random.Range(0, m_ChanceOfCloudSpawn) <= m_ChanceOfCloudSpawn/3.0f) {
				spawnCloud(Random.Range(0.0f,1.0f));
			}
		}
	}

	//time through current animation
	private void spawnCloud(float a_Time) {
		GameObject cloud = new GameObject("Cloud", typeof(RectTransform));
		Image img = cloud.AddComponent<Image>();
		img.sprite = m_Clouds[Random.Range(0, m_Clouds.Length)];
		Animator ani = cloud.AddComponent<Animator>();
		ani.runtimeAnimatorController = m_Animator;
		ani.Play("New Animation", 0, a_Time);
		ani.speed = Random.Range(0.5f, 1.5f);
		cloud.transform.SetParent(transform, false);
		RectTransform rt = (RectTransform)cloud.transform;
		rt.sizeDelta = new Vector2(400, 150);
	}
}

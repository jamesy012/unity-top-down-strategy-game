﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {

	public float m_Speed = 5;

	public bool m_EnableEdgeScroll = false;

	public float m_MinZoom = 2.0f;
	public float m_MaxZoom = 17.0f;
	public float m_ZoomSpeed = 75.0f;

	[Range(0.0f, 0.5f)]
	public float m_PercentageEdgeSize = 0.02f;

	// Update is called once per frame
	void Update () {
		float x = Input.GetAxis("Horizontal");
		float y = Input.GetAxis("Vertical");
		float scroll = -Input.GetAxis("Mouse ScrollWheel");


		if (OverUI.m_MOUSEOVER.Count == 0 && m_EnableEdgeScroll) {
			float screenX = Screen.width * m_PercentageEdgeSize;
			float screenY = Screen.height * m_PercentageEdgeSize;

			if (Input.mousePosition.x > Screen.width - screenX) {
				x += 1.0f;
			} else if (Input.mousePosition.x < screenX) {
				x -= 1.0f;
			}
			if (Input.mousePosition.y > Screen.height - screenY) {
				y += 1.0f;
			} else if (Input.mousePosition.y < screenY) {
				y -= 1.0f;
			}
		}

		Vector3 movement = new Vector3(x, y, 0) * m_Speed * Time.deltaTime;

		transform.position = transform.position + movement;


		Camera.main.orthographicSize += scroll * m_ZoomSpeed * Time.deltaTime;
		Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, m_MinZoom, m_MaxZoom);

		limitToMapBounds();

	}

	private void limitToMapBounds() {
		Map map = Map.m_CurrentMap;
		if(map == null) {
			return;
		}
		Vector3 localMapPos = transform.position - map.transform.position;


		localMapPos.x = Mathf.Clamp(localMapPos.x, 0, map.m_Width);
		localMapPos.y = Mathf.Clamp(localMapPos.y, -map.m_Height, 0);

		transform.position = localMapPos + map.transform.position;
	}
}

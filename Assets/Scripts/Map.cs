﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour {

	public ImageList m_ImageList;

	private Tile[,] m_Tiles;
	public int m_Width;
	public int m_Height;

	public static Map m_CurrentMap = null;
	public bool m_DefaultMap = false;

	public enum Directions {
		Up,
		Right,
		Down,
		Left,
		UpRight,
		DownRight,
		DownLeft,
		UpLeft,
		Still
	};

	public void Awake() {
		if (m_DefaultMap) {
			if (m_CurrentMap != null) {
				Debug.LogWarning("MULTIPLE DEFAULT MAPS");
			}
			m_CurrentMap = this;
		}
	}

	public void OnDrawGizmosSelected() {
		Gizmos.color = new Color(1, 0, 0);
		Gizmos.DrawSphere(transform.position - new Vector3(1, -1) / 2.0f, 0.5f);
		Gizmos.DrawSphere(transform.position + new Vector3(m_Width,-m_Height) - new Vector3(1, -1) / 2.0f, 0.5f);
	}

	public void setSize(int a_Width, int a_Height) {
		m_Tiles = new Tile[a_Width, a_Height];
		m_Width = a_Width;
		m_Height = a_Height;
	}



	public void addTile(int a_X, int a_Y, Tile a_Tile) {
		if (!inBounds(a_X, a_Y)) {
			return;
		}
		m_Tiles[a_X, a_Y] = a_Tile;
	}

	public void addTile(Tile a_Tile) {
		addTile(a_Tile.m_Tx, a_Tile.m_Ty, a_Tile);
	}

	public Tile getTile(int a_X, int a_Y) {
		if (!inBounds(a_X, a_Y)) {
			return null;
		}
		return m_Tiles[a_X, a_Y];
	}

	public Tile getTileFromPosition(float a_X, float a_Y) {

		int x = Mathf.RoundToInt(a_X - transform.position.x);
		int y = -Mathf.RoundToInt(a_Y - transform.position.y);

		if (!inBounds(x, y)) {
			return null;
		}
		return m_Tiles[x, y];
	}

	public void clear() {
		if(m_Tiles == null) {
			return;
		}
		for (int y = 0; y < m_Height; y++) {
			for (int x = 0; x < m_Width; x++) {
				Destroy(m_Tiles[x, y].gameObject);
				m_Tiles[x, y] = null;
			}
		}
	}

	public bool inBounds(int a_X, int a_Y) {
		if ((a_X >= m_Width || a_X < 0) || (a_Y >= m_Height || a_Y < 0)) {
			return false;
		}
		return true;
	}

	public static float getDistFromDirection(Directions a_Dir) {
		float dist = 0;
		switch (a_Dir) {
			case Directions.Up:
			case Directions.Right:
			case Directions.Down:
			case Directions.Left:
				dist = 1.0f;
				break;
			case Directions.UpRight:
			case Directions.DownRight:
			case Directions.DownLeft:
			case Directions.UpLeft:
				dist = 1.41f;
				break;
			case Directions.Still:
				dist = 0.0f;
				break;
		}
		return dist;
	}

	public static Vector2 getVectorFromDirection(Directions a_Dir) {
		Vector2 dist = new Vector2(0, 0);
		switch (a_Dir) {
			case Directions.Up:
				dist.y = 1;
				break;
			case Directions.Right:
				dist.x = 1;
				break;
			case Directions.Down:
				dist.y = -1;
				break;
			case Directions.Left:
				dist.x = -1;
				break;
			case Directions.UpRight:
				dist.y = 1;
				dist.x = 1;
				break;
			case Directions.DownRight:
				dist.y = -1;
				dist.x = 1;
				break;
			case Directions.DownLeft:
				dist.y = -1;
				dist.x = -1;
				break;
			case Directions.UpLeft:
				dist.y = 1;
				dist.x = -1;
				break;
			case Directions.Still:
				dist.y = 0;
				dist.x = 0;
				break;
		}
		return dist;
	}

	public static Vector2 getVectorFromDirectionNormalized(Directions a_Dir) {
		Vector2 dist = new Vector2(0, 0);
		switch (a_Dir) {
			case Directions.Up:
				dist.y = 1;
				break;
			case Directions.Right:
				dist.x = 1;
				break;
			case Directions.Down:
				dist.y = -1;
				break;
			case Directions.Left:
				dist.x = -1;
				break;
			//Vector2(1,1).normalized = (0.7,0.7)
			case Directions.UpRight:
				dist.y = 0.7f;
				dist.x = 0.7f;
				break;
			case Directions.DownRight:
				dist.y = -0.7f;
				dist.x = 0.7f;
				break;
			case Directions.DownLeft:
				dist.y = -0.7f;
				dist.x = -0.7f;
				break;
			case Directions.UpLeft:
				dist.y = 0.7f;
				dist.x = -0.7f;
				break;
			case Directions.Still:
				dist.y = 0;
				dist.x = 0;
				break;
		}
		return dist;
	}

	public static Directions getOppositeDirection(Directions a_Dir) {
		Directions dir = Directions.Still;
		switch (a_Dir) {
			case Directions.Up:
				dir = Directions.Down;
				break;
			case Directions.Right:
				dir = Directions.Left;
				break;
			case Directions.Down:
				dir = Directions.Up;
				break;
			case Directions.Left:
				dir = Directions.Right;
				break;
			case Directions.UpRight:
				dir = Directions.DownLeft;
				break;
			case Directions.DownRight:
				dir = Directions.UpLeft;
				break;
			case Directions.DownLeft:
				dir = Directions.UpRight;
				break;
			case Directions.UpLeft:
				dir = Directions.DownRight;
				break;
			case Directions.Still:
				dir = Directions.Still;
				break;
		}
		return dir;
	}

	public static List<Directions> getDirectionFromPath(List<Tile> a_Path) {
		List<Directions> path = new List<Directions>();
		for(int i = a_Path.Count-2; i >= 0; i--) {
			Directions dir = findInNeighbor(a_Path[i + 1], a_Path[i]);
			path.Add(dir);
		}
		if(path.Count == 0) {
			return null;
		}
		return path;
	}

	public static Directions findInNeighbor(Tile a_Start,Tile a_End) {
		Directions direction = Directions.Still;
		for(int i = 0; i < 4; i++) {
			Tile neigbour = a_Start.getNeighbour((Directions)i);
			if(neigbour == a_End) {
				direction = (Directions)i;
			}
		}
		return direction;
	}

}

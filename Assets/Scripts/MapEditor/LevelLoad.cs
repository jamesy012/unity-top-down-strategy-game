﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LevelLoad : MonoBehaviour {

	public string m_FilePath;
	private string m_FullPath;

	public Map m_Map;

	public TextAsset m_TextAsset; // Gets assigned through code. Reads the file.

	public bool m_LoadMapOnStart = true;

	public void Awake() {
		m_FullPath = "Assets/" + m_FilePath + m_TextAsset.name + ".txt";

		if (m_LoadMapOnStart) {
			load();
		} else {
			int width = 50;
			int height = 10;
			m_Map.setSize(width, height);
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					GameObject gm = new GameObject();
					Tile tile = gm.AddComponent<Tile>();
					tile.setUp(m_Map, 1, x, y);
				}
			}
		}

	}

	public void load() {
		string fileText;
		//ability to save and load without restarting in the editor
#if UNITY_EDITOR
		fileText = File.ReadAllText(m_FullPath);
#else
		fileText = m_TextAsset.text;
#endif

		m_Map.clear();
		string[] text = fileText.Split(',');
		int width = int.Parse(text[0]);
		int height = int.Parse(text[1]);
		m_Map.setSize(width, height);
		int index = 2;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int number = int.Parse(text[index]);
				//if (number == -1) {
				//	index++;
				//	continue;
				//}
				GameObject gm = new GameObject();
				Tile tile = gm.AddComponent<Tile>();
				tile.setUp(m_Map, number, x, y);

				index++;
			}
		}

	}

	public void save() {
		string level;
		level = m_Map.m_Width + ",\t\t" + m_Map.m_Height + ",\n";

		for (int y = 0; y < m_Map.m_Height; y++) {
			for (int x = 0; x < m_Map.m_Width; x++) {
				Tile tile = m_Map.getTile(x, y);
				level += tile.m_Tile;
				if (tile.m_Tile >= 100) {
					level += ",\t";
				} else {
					level += ",\t\t";
				}
			}
			level += "\n";
		}

		File.WriteAllText(m_FullPath, level);
	}

	//void AppendString(string appendString) {
	//
	//	print(asset.name);
	//	print(asset.text);
	//
	//	File.WriteAllText(m_FullPath, File.ReadAllText(m_FullPath) +  " TEST");
	//}

}
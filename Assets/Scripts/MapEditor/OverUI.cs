﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OverUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public static List<OverUI> m_MOUSEOVER = new List<OverUI>();
	/// <summary>
	/// this is here because when clicking on a new tile is checks if m_MOUSEOVER is empty
	/// but we remove it from the list so it's empty when we click so this will be true if we don't want it to use that click
	/// </summary>
	public static bool m_NEXTCLICK = false;

	public void OnPointerEnter(PointerEventData eventData) {
		m_MOUSEOVER.Add(this);
	}

	public void OnPointerExit(PointerEventData eventData) {
		m_MOUSEOVER.Remove(this);
	}
}

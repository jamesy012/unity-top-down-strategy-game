﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TilePick : MonoBehaviour,IPointerDownHandler {

	public int m_TileIndex;
	public TileSelect m_TileSelect;

	public void OnPointerDown(PointerEventData eventData) {
		transform.parent.parent.parent.gameObject.SetActive(false);
		OverUI.m_NEXTCLICK = true;
		OverUI.m_MOUSEOVER.Clear();
		//OverUI.m_MOUSEOVER.Remove(transform.parent.parent.parent.GetComponent<OverUI>());
		m_TileSelect.selectTile(m_TileIndex);
	}
}

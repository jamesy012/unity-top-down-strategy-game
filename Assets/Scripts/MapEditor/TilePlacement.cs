﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilePlacement : MonoBehaviour {

	public Map m_Map;

	private TileSelect m_TileSelect;
	private SpriteRenderer m_SR;
	private bool m_OverMap = false;

	private int lastX;
	private int lastY;

	private bool m_NextClick = false;

	// Use this for initialization
	void Start() {
		m_TileSelect = GetComponent<TileSelect>();
		m_SR = GetComponent<SpriteRenderer>();
		m_SR.enabled = false;
	}

	// Update is called once per frame
	void Update() {

		if (OverUI.m_MOUSEOVER.Count != 0) {
			m_SR.enabled = false;
			m_OverMap = false;
			return;
		}

		Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - m_Map.transform.position;
		//print(mousePos);
		int tiledX = Mathf.RoundToInt(mousePos.x);
		int tiledY = Mathf.RoundToInt(mousePos.y);

		if ((tiledX == lastX && tiledY == lastY) && !Input.GetMouseButtonDown(0)) {
			return;
		}
		lastX = tiledX;
		lastY = tiledY;

		if ((tiledX >= m_Map.m_Width || tiledX < 0) || (-tiledY >= m_Map.m_Height || -tiledY < 0)) {
			m_SR.enabled = false;
			m_OverMap = false;
			return;
		} else {
			m_SR.enabled = true;
			m_OverMap = true;
		}

		transform.position = new Vector3(tiledX, tiledY, 0) + m_Map.transform.position;

		if (OverUI.m_NEXTCLICK) {
			if (m_NextClick) {
				if (Input.GetMouseButtonDown(0)) {
					OverUI.m_NEXTCLICK = false;
					m_NextClick = false;
				} else {
					return;
				}
			} else {
				m_NextClick = true;
				return;
			}
		}

		if (m_OverMap && Input.GetMouseButton(0)) {
			Tile tile = m_Map.getTile(tiledX, -tiledY);
			if (tile == null) {
				return;
			}
			tile.setTile(m_TileSelect.m_SelectedTile);
		}
	}
}

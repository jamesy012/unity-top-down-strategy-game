﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileSelect : MonoBehaviour {

	public Image m_Image;
	public GameObject m_TileSelection;

	public int m_SelectedTile = 0;

	private TilePlacement m_TilePlacement;

	// Use this for initialization
	void Start () {
		m_TilePlacement = GetComponent<TilePlacement>();
		updateTile();
		addButtons();
	}

	void addButtons() {
		GameObject[] horizontal = new GameObject[16];
		for(int i = 0; i < horizontal.Length; i++) {
			horizontal[i] = new GameObject();
			horizontal[i].transform.name = "horizontal row " + i;
			horizontal[i].transform.SetParent(m_TileSelection.transform);
			HorizontalLayoutGroup hlg = horizontal[i].AddComponent<HorizontalLayoutGroup>();
			hlg.childControlHeight = hlg.childControlWidth = true;
			for(int q = 0; q < 16; q++) {
				GameObject gm = new GameObject();
				gm.AddComponent<RectTransform>();
				gm.transform.SetParent(horizontal[i].transform);
				TilePick tp = gm.AddComponent<TilePick>();
				tp.m_TileIndex = (16 * i) + q;
				tp.m_TileSelect = this;
				Image img = gm.AddComponent<Image>();
				img.sprite = m_TilePlacement.m_Map.m_ImageList.m_Tiles[(16*i) + q];
				gm.transform.name = "SelectableTile: " + tp.m_TileSelect;
			}
		}

	}

	public void selectTile(int a_Index) {
		m_SelectedTile = a_Index;
		updateTile();
	}

	public void updateTile() {
		m_Image.sprite = m_TilePlacement.m_Map.m_ImageList.m_Tiles[m_SelectedTile];
	}

}

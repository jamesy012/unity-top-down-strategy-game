﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIToggle : MonoBehaviour {

	public bool m_ToggleOnStart = false;

	public void Start() {
		if (!m_ToggleOnStart) {
			return;
		} 

		UnityEngine.UI.Button button = GetComponent<UnityEngine.UI.Button>();

		if (button != null) {
			button.onClick.Invoke();
		}
	}

	public void toggleVisablity(GameObject a_GO) {
		a_GO.SetActive(!a_GO.activeSelf);
	}
}

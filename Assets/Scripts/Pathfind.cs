﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfind : MonoBehaviour {

	class Node {
		public Tile tile = null;
		public float gCost = 999999;
		public float fCost = 999999;
		public float hCost = 0;
		public Node prev = null;
		public bool inStack = false;
		public bool isTraversed = false;
		public int tileDistance = 0;

		public static int fCostSort(Node a, Node b) {
			return a.fCost.CompareTo(b.fCost);
		}
	}

	//all nodes
	private static List<Node> m_Nodes = null;

	private static Map m_CurrMap = null;

	public static List<Tile> getTilesInArea(Tile a_Tile, float a_Range) {

		checkNodeListUpdate();
		Vector2 tilePos = new Vector2(a_Tile.m_Tx, a_Tile.m_Ty);
		List<Node> queue = new List<Node>();
		List<Tile> tilesInRange = new List<Tile>();

		for (int i = 0; i < m_Nodes.Count; i++) {
			m_Nodes[i].isTraversed = false;
			m_Nodes[i].inStack = false;
		}

		queue.Add(m_Nodes[(m_CurrMap.m_Width * a_Tile.m_Ty) + a_Tile.m_Tx]);

		while (queue.Count != 0) {

			Node node = queue[0];
			queue.RemoveAt(0);

			if (node.tile != a_Tile) {
				tilesInRange.Add(node.tile);
			}

			node.isTraversed = true;
			node.inStack = false;

			for (int i = 0; i < 4; i++) {
				Tile tileNeighbor = node.tile.getNeighbour((Map.Directions)i);
				if (tileNeighbor == null) {
					continue;
				}
				Node neighbor = m_Nodes[(m_CurrMap.m_Width * tileNeighbor.m_Ty) + tileNeighbor.m_Tx];

				if (neighbor.isTraversed || node.inStack) {//has it's been traversed or is currently in the queue
					continue;
				}

				float dist = Vector2.Distance(tilePos, new Vector2(tileNeighbor.m_Tx, tileNeighbor.m_Ty));

				//if inside range
				if (a_Range >= dist) {
					queue.Add(neighbor);
					neighbor.inStack = true;
				}

			}

		}

		if (tilesInRange.Count == 0) {
			return null;
		}
		return tilesInRange;
	}



	/// <summary>
	/// returns all the tiles near a_Tile within a_Range
	/// will not go through enemy units or walls
	/// </summary>
	/// <param name="a_Tile">starting tile</param>
	/// <param name="a_Range">range in tiles</param>
	/// <returns>list of tiles in range</returns>
	public static List<Tile> getTilesInTilesArea(Tile a_Tile, int a_Range) {
		//if the map has changed then update it
		checkNodeListUpdate();

		//list of tiles that we need to check and are in range
		List<Node> queue = new List<Node>();
		//tiles that have been checked and are in range
		List<Tile> tilesInRange = new List<Tile>();

		//reset data that this function will use
		for (int i = 0; i < m_Nodes.Count; i++) {
			m_Nodes[i].isTraversed = false;
			m_Nodes[i].inStack = false;
			m_Nodes[i].tileDistance = 0;
			m_Nodes[i].tile.m_NextToEnemy = false;
		}

		//add the tile from a_Tile to the queue
		//have to convert tile into single array coords
		queue.Add(m_Nodes[(m_CurrMap.m_Width * a_Tile.m_Ty) + a_Tile.m_Tx]);

		//while there is something in the queue
		while (queue.Count != 0) {
			//get starting node
			Node node = queue[0];
			//remove that node from the list
			queue.RemoveAt(0);

			//if it's not the starting tile then
			if (node.tile != a_Tile) {
				tilesInRange.Add(node.tile);
			}

			//set the node as traversed
			node.isTraversed = true;
			//and say it's not in the stack anymore
			node.inStack = false;

			//if there is a unit on this tile and it's not the starting tile (due to being that where we want to path from)
			if (node.tile.m_UnitOnTile != null && node.tile != a_Tile) {
				continue;
			}

			//go through all adjacent tiles
			for (int i = 0; i < 4; i++) {
				//get tile from node
				Tile tileNeighbor = node.tile.getNeighbour((Map.Directions)i);
				//if it was a edge tile then the neighbor will be null
				//if it is then don't look at this tile since it wont have any data and give a null reference error
				if (tileNeighbor == null) {
					continue;
				}

				//check if enemy square in this tile
				//get unit from tile
				Unit unitOnTile = tileNeighbor.m_UnitOnTile;
				//tile can not have unit sometimes so we check if it has a unit
				if (unitOnTile != null) {
					//check to see if it's not on the team
					if (unitOnTile.m_PlayerTeam != UnitSelect.m_SelectedUnit.m_PlayerTeam) {
						//set this tile to say it's next to a enemy
						node.tile.m_NextToEnemy = true;
						tileNeighbor.m_NextToEnemy = true;
					}
				}

				//if there is a wall on this tile then don't add it
				if (isWallHere(tileNeighbor)) {
					continue;
				}

				//gets the neighbors node by converting position into a single array position
				Node neighbor = m_Nodes[(m_CurrMap.m_Width * tileNeighbor.m_Ty) + tileNeighbor.m_Tx];

				//if this node has been traversed or is currently in the stack then don't add it again
				if (neighbor.isTraversed || neighbor.inStack) {
					continue;
				}

				//if this tile is out of range then don't bother adding it to the queue
				if (node.tileDistance >= a_Range) {
					continue;
				}

				//all checks have been passes, this tile is free to be added and for us to check it's neighbors
				queue.Add(neighbor);
				neighbor.inStack = true;
				neighbor.tileDistance = node.tileDistance + 1;

			}

		}
		//if there were no tiles added then return null
		if (tilesInRange.Count == 0) {
			return null;
		}

		return tilesInRange;
	}

	public static List<Tile> pathToTile(Tile a_Start, Tile a_End, int a_Range = -1) {
		//dont run of either tile is null
		if (a_End == null || a_Start == null) {
			return null;
		}
		//dont run if the path is in a wall
		if (isWallHere(a_End)) {
			return null;
		}

		checkNodeListUpdate();

		List<Node> queue = new List<Node>();
		List<Tile> path = new List<Tile>();

		for (int i = 0; i < m_Nodes.Count; i++) {
			m_Nodes[i].isTraversed = false;
			m_Nodes[i].inStack = false;
			m_Nodes[i].prev = null;
			m_Nodes[i].tileDistance = 0;
			m_Nodes[i].fCost = m_Nodes[i].gCost = 999999;
			m_Nodes[i].hCost = 0;
		}

		Node start = m_Nodes[(m_CurrMap.m_Width * a_Start.m_Ty) + a_Start.m_Tx];
		Node end = m_Nodes[(m_CurrMap.m_Width * a_End.m_Ty) + a_End.m_Tx];

		start.gCost = 0;
		start.prev = start;

		queue.Add(start);

		bool foundEnd = false;

		while (queue.Count != 0) {
			//print("A*");
			queue.Sort(Node.fCostSort);

			Node node = queue[0];
			queue.RemoveAt(0);

			node.isTraversed = true;
			node.inStack = false;

			if (node == end) {
				break;
			}

			if (foundEnd) {
				continue;
			}

			//get neighbors 
			for (int i = 0; i < 4; i++) {
				Tile tileNeighbor = node.tile.getNeighbour((Map.Directions)i);
				if (tileNeighbor == null) {
					continue;
				}
				if (isWallHere(tileNeighbor)) {
					continue;
				}
				if (tileNeighbor.m_UnitOnTile != null && tileNeighbor != a_End) {
					continue;
				}
				Node neighbor = m_Nodes[(m_CurrMap.m_Width * tileNeighbor.m_Ty) + tileNeighbor.m_Tx];

				if (neighbor.tile != a_End) {//if not end tile
					if (neighbor.isTraversed || neighbor.inStack) {//has it's been traversed or is currently in the queue
						continue;
					}
				} else {
					foundEnd = true;
				}

				//current node + cost to travel in direction
				float calcGCost = node.gCost + Map.getDistFromDirection((Map.Directions)i);

				if (neighbor.hCost == 0) {
					neighbor.hCost = Vector2.Distance(new Vector2(a_End.m_Tx, a_End.m_Ty), new Vector2(tileNeighbor.m_Tx, tileNeighbor.m_Ty));
				}
				float calcFCost = calcGCost + neighbor.hCost;

				//if it cost less to travel here using this node then the current
				if (calcFCost < neighbor.fCost) {

					neighbor.prev = node;
					neighbor.fCost = calcFCost;
					neighbor.gCost = calcGCost;

					queue.Add(neighbor);
					neighbor.inStack = true;
					neighbor.tileDistance = node.tileDistance + 1;

				}

			}
		}

		//did the final node get reached
		if (end.isTraversed) {
			Node currNode = end; // travel back

			//continue until were at the starting node
			while (currNode != start) {
				path.Add(currNode.tile);
				currNode = currNode.prev;
			}

			//add starting node
			path.Add(a_Start);

		} else {
			return null;
		}

		return path;
	}

	private static bool isWallHere(Tile a_Tile) {
		if (a_Tile.m_Tile == 164) {
			return true;
		}
		//if (a_Tile.m_UnitOnTile != null) {
		//	return true;
		//}
		return false;
	}

	public static void updateNodeList() {
		m_Nodes = new List<Node>(m_CurrMap.m_Width * m_CurrMap.m_Height);
		for (int y = 0; y < m_CurrMap.m_Height; y++) {
			for (int x = 0; x < m_CurrMap.m_Width; x++) {
				Node node = new Node();
				node.tile = m_CurrMap.getTile(x, y);
				m_Nodes.Add(node);
			}
		}
	}

	private static void checkNodeListUpdate() {
		bool update = false;

		if (m_CurrMap == null || m_CurrMap != Map.m_CurrentMap) {
			m_CurrMap = Map.m_CurrentMap;
			update = true;
		} else if (m_Nodes == null) {
			update = true;
		}

		if (update) {
			updateNodeList();
		}
	}

	public static Tile getFirstNonWall(Tile a_Tile) {

		checkNodeListUpdate();

		List<Node> queue = new List<Node>();

		for (int i = 0; i < m_Nodes.Count; i++) {
			m_Nodes[i].isTraversed = false;
			m_Nodes[i].inStack = false;
		}

		queue.Add(m_Nodes[(m_CurrMap.m_Width * a_Tile.m_Ty) + a_Tile.m_Tx]);

		while (queue.Count != 0) {

			Node node = queue[0];
			queue.RemoveAt(0);

			node.isTraversed = true;
			node.inStack = false;

			for (int i = 0; i < 4; i++) {
				Tile tileNeighbor = node.tile.getNeighbour((Map.Directions)i);
				if (tileNeighbor == null) {
					continue;
				}
				Node neighbor = m_Nodes[(m_CurrMap.m_Width * tileNeighbor.m_Ty) + tileNeighbor.m_Tx];

				if (neighbor.isTraversed || neighbor.inStack) {//has it's been traversed or is currently in the queue
					continue;
				}

				if (isWallHere(tileNeighbor)) {
					queue.Add(neighbor);
					neighbor.inStack = true;
					continue;
				}

				//else this is not a wall
				return tileNeighbor;




			}

		}

		return null;
	}

}

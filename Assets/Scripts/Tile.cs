﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

	private SpriteRenderer m_SR;
	private Map m_Map;

	public int m_Tx, m_Ty;

	public int m_Tile = -1;



	/// <summary>
	/// this is set by Pathfind.cs,
	/// will update to what ever unit was selected
	/// and only update in the range of the pathfind
	/// </summary>
	public bool m_NextToEnemy = false;
	public Unit m_UnitOnTile = null;

	//public bool m_Wall = false;

	[SerializeField]
	protected Tile[] m_Neighbours = new Tile[4];

	private static int m_TilesCounter = 0;

	public void Start() {

	}

	public void setMapHolder(Map a_Map) {
		m_Map = a_Map;
	}

	public void setTile(int a_Tile) {
		if (a_Tile > m_Map.m_ImageList.m_Tiles.Length) {
			a_Tile = -1;
		}
		m_Tile = a_Tile;
		transform.name = "Tile: " + m_TilesCounter++;

		if (a_Tile == -1) {
			m_SR.sprite = null;
			return;
		}
		m_SR.sprite = m_Map.m_ImageList.m_Tiles[a_Tile];
	}

	public void setUp(Map a_Map, int a_Number, int a_X, int a_Y) {
		m_SR = gameObject.AddComponent<SpriteRenderer>();
		m_Tx = a_X;
		m_Ty = a_Y;
		setMapHolder(a_Map);
		setTile(a_Number);
		transform.position = new Vector3(a_X, -a_Y, 0) + m_Map.transform.position;
		transform.parent = m_Map.transform;
		gameObject.layer = LayerMask.NameToLayer("Tiles");
		m_Map.addTile(this);

		gameObject.AddComponent<BoxCollider>();
		updateNeighbours();
	}

	private void updateNeighbours() {
		for (int i = 0; i < 4; i++) {
			if (m_Neighbours[i] != null) {
				continue;
			}
			Map.Directions dir = (Map.Directions)i;
			Map.Directions opposite = Map.getOppositeDirection(dir);
			Tile neighbour = null;
			switch (dir) {
				case Map.Directions.Up:
					neighbour = m_Map.getTile(m_Tx, m_Ty - 1);
					break;
				case Map.Directions.Down:
					neighbour = m_Map.getTile(m_Tx, m_Ty + 1);
					break;
				case Map.Directions.Right:
					neighbour = m_Map.getTile(m_Tx + 1, m_Ty);
					break;
				case Map.Directions.Left:
					neighbour = m_Map.getTile(m_Tx - 1, m_Ty);
					break;

				//these are not used
				case Map.Directions.UpRight:
					neighbour = m_Map.getTile(m_Tx + 1, m_Ty - 1);
					break;
				case Map.Directions.DownRight:
					neighbour = m_Map.getTile(m_Tx + 1, m_Ty + 1);
					break;
				case Map.Directions.DownLeft:
					neighbour = m_Map.getTile(m_Tx - 1, m_Ty + 1);
					break;
				case Map.Directions.UpLeft:
					neighbour = m_Map.getTile(m_Tx - 1, m_Ty - 1);
					break;
			}
			if (neighbour == null) {
				continue;
			}
			m_Neighbours[i] = neighbour;
			neighbour.m_Neighbours[(int)opposite] = this;
		}
	}

	public Tile getNeighbour(Map.Directions a_Direction) {
		//only let through Up,Down,Right,Left
		switch (a_Direction) {
			case Map.Directions.Up:
			case Map.Directions.Right:
			case Map.Directions.Down:
			case Map.Directions.Left:
				break;
			default:
				return null;
		}
		return m_Neighbours[(int)a_Direction];
	}
}

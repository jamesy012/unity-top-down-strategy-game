﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {

	public float m_Speed = 5.0f;

	public Tile m_CurrTile;

	public bool m_PlayerTeam;

	private Vector2 m_LastPos;
	private Map.Directions[] m_Path;// = { Map.Directions.Down, Map.Directions.Down, Map.Directions.Right, Map.Directions.Down, Map.Directions.Up, Map.Directions.UpRight, Map.Directions.UpLeft, Map.Directions.Left };

	private int m_PathIndex = 0;

	[Range(1, 10)]
	public int m_MovementRange = 3;

	public static bool m_UnitMoving = false;
	public bool m_ThisUnitMoving = false;

	
	public int m_Health = 2;
	public int m_MaxHealth = 2;

	public int m_AttackStrength = 1;

	// Use this for initialization
	void Start() {
		m_Health = m_MaxHealth;
		snapToTile();
		m_LastPos = transform.position;
	}

	// Update is called once per frame
	void Update() {
		if (m_Path == null || !m_ThisUnitMoving) {
			return;
		}
		Vector2 movement = Map.getVectorFromDirectionNormalized(m_Path[m_PathIndex]);
		transform.position += new Vector3(movement.x, movement.y, 0) * m_Speed * Time.deltaTime;
		float dist = Vector2.Distance(m_LastPos, transform.position);
		if (dist >= Map.getDistFromDirection(m_Path[m_PathIndex])) {
			m_LastPos = transform.position;
			snapToTile();
			//m_PathIndex = (m_PathIndex + 1) % m_Path.Length;
			m_PathIndex++;
			if (m_PathIndex >= m_Path.Length) {
				m_Path = null;
				m_UnitMoving = m_ThisUnitMoving = false;
			}

		}
	}

	private void snapToTile() {
		Tile tileOn = Map.m_CurrentMap.getTileFromPosition(transform.position.x, transform.position.y);
		if (tileOn == null) {
			transform.position = m_CurrTile.transform.position;
			return;
		}
		transform.position = tileOn.transform.position;

		//if we were just on a tile, then tell that tile we are no longer there
		if (m_CurrTile != null) {
			m_CurrTile.m_UnitOnTile = null;
		}

		m_CurrTile = tileOn;

		//tell tile we are on it now
		m_CurrTile.m_UnitOnTile = this;
	}

	public void setPath(List<Map.Directions> a_Path) {
		m_PathIndex = 0;
		m_Path = a_Path.ToArray();
		m_UnitMoving = m_ThisUnitMoving = true;

		//if we were just on a tile, then tell that tile we are no longer there
		if (m_CurrTile != null) {
			m_CurrTile.m_UnitOnTile = null;
		}
	}

	public void attack(int a_AttackStrength) {
		m_Health -= a_AttackStrength;
		//if unit is dead
		if(m_Health <= 0) {
			//remove reference to this tile
			if(m_CurrTile != null) {
				m_CurrTile.m_UnitOnTile = null;
			}
			Destroy(gameObject);
		}
	}


}

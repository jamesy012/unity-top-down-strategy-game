﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPathing : MonoBehaviour {

	/// <summary>
	/// a simple white square 
	/// </summary>
	public Sprite m_WhiteSquareSprite;

	private Sprite m_WhiteSprite;

	/// <summary>
	/// list of tiles that the selected unit can move to
	/// created by unitSelected, used by unitPath
	/// </summary>
	private List<Tile> m_TilesNearBy;

	/// <summary>
	/// current tile the mouse is over.
	/// Used by unitPath
	/// </summary>
	private Tile m_CurrentMouseTile;

	/// <summary>
	/// path the unit will take if the user tells it to move.
	/// created by unitPath, used by moveUnit
	/// </summary>
	private List<Tile> m_Path;

	/// <summary>
	/// a reference to the tile this unit will be attacking
	/// </summary>
	private Tile m_AttackTile;

	private bool m_AttackAfterWalk = false;

	// Use this for initialization
	void Start() {
		UnitSelect us = GetComponent<UnitSelect>();
		us.m_UnitSelected.AddListener(unitSelected);
		us.m_UnitDeselected.AddListener(unitDeselected);

		Texture2D tex = new Texture2D(1, 1);
		tex.SetPixel(0, 0, Color.white);
		tex.Apply();

		m_WhiteSprite = Sprite.Create(tex, new Rect(0, 0, 1, 1), new Vector2(0.5f, 0.5f), 1);
	}

	public void Update() {
		//if there is a unit moving then don't update anything
		if (Unit.m_UnitMoving) {
			return;
		}
		if (BattleScreen.m_BattleScreen != null) {
			if (BattleScreen.m_BattleScreen.m_IsOpen) {
				return;
			}
		}

		if (m_AttackAfterWalk) {
			m_AttackAfterWalk = false;
			print("ATTACK!!!");
			BattleScreen.m_BattleScreen.open(UnitSelect.m_SelectedUnit,m_AttackTile.m_UnitOnTile);
		}

		//if there is no unit selected then don't update anything
		if (UnitSelect.m_SelectedUnit == null) {
			return;
		}

		//if the tiles nearby list is empty then create the list
		if (m_TilesNearBy == null) {
			unitSelected();
		} else {//else show where the unit will move if the user was to click now
			unitPath();
		}

		//if the player right clicked then move the unit to the selected square
		if (Input.GetMouseButtonDown(1)) {
			moveUnit();
		}


	}

	/// <summary>
	/// creates the path from selected unit tile on where the mouse is 
	/// </summary>
	//TODO perhaps we do not attack using unitPath, instead a ui element will say Attack/Items/Wait and you pick from them
	private void unitPath() {
		//TODO if next to enemy and then you move onto the enemy, keep that same path and attack enemy from direction that the mouse was on before this tile
		Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Tile mouseTile = Map.m_CurrentMap.getTileFromPosition(mousePos.x, mousePos.y);

		//only update if we change tile
		if (mouseTile != m_CurrentMouseTile) {
			Tile oldTile = m_CurrentMouseTile;

			m_CurrentMouseTile = mouseTile;


			//clear the last path
			for (int i = 0; i < transform.childCount; i++) {
				Destroy(transform.GetChild(i).gameObject);
			}

			//reset path
			m_Path = new List<Tile>();

			//if the area we can path to doesn't contain the current tile the mouse is over then return 
			if (!m_TilesNearBy.Contains(m_CurrentMouseTile)) {
				return;
			}

			//flag to check if this path will result in an attack or not
			bool normalPath = true;

			//check if mouseTile is now on a enemy
			if (oldTile != null) {
				if (oldTile.m_NextToEnemy && m_CurrentMouseTile.m_UnitOnTile != null) {
					//if the current tile has a enemy thats on the other team
					if (m_CurrentMouseTile.m_UnitOnTile.m_PlayerTeam != UnitSelect.m_SelectedUnit.m_PlayerTeam) {
						normalPath = false;
					}
				}
			}

			//check if mouseTile is over ally?
			if (m_CurrentMouseTile.m_UnitOnTile != null) {
				if (m_CurrentMouseTile.m_UnitOnTile.m_PlayerTeam == UnitSelect.m_SelectedUnit.m_PlayerTeam) {
					return;
				}
			}


			//path generation
			if (normalPath) {
				//print("Normal");
				m_Path = Pathfind.pathToTile(UnitSelect.m_SelectedUnit.m_CurrTile, m_CurrentMouseTile);

				m_AttackTile = null;


				if (m_CurrentMouseTile.m_UnitOnTile != null) {
					//if the current tile has a enemy thats on the other team
					if (m_CurrentMouseTile.m_UnitOnTile.m_PlayerTeam != UnitSelect.m_SelectedUnit.m_PlayerTeam) {
						m_Path = null;
						m_AttackTile = m_CurrentMouseTile;
					}
				}
			} else {
				//print("Attack");
				m_Path.AddRange(Pathfind.pathToTile(UnitSelect.m_SelectedUnit.m_CurrTile, oldTile));

				m_AttackTile = m_CurrentMouseTile;

				//if the tile is next to this tile then just attack
				if (m_Path != null) {
					if (m_Path.Count == 1) {
						m_Path = null;
					}
				}

				//if this is to far to move then don't make the path there
				if (m_Path != null) {
					if (m_Path.Count > UnitSelect.m_SelectedUnit.m_MovementRange) {
						m_Path = null;
						//also say we wont attack that tile
						m_AttackTile = null;
					}
				}

			}

			//if no path, return
			if (m_Path == null) {
				return;
			}

			//show path to user
			//create color
			Color col = new Color(0, 1, 0, 0.5f);
			if (m_CurrentMouseTile.m_UnitOnTile != null) {
				if (m_CurrentMouseTile.m_UnitOnTile.m_PlayerTeam != UnitSelect.m_SelectedUnit.m_PlayerTeam) {
					col = new Color(1, 0, 0, 0.5f);
					//m_UnitToAttack = m_CurrentMouseTile.m_UnitOnTile;
				}
			}
			//gen visual path GameObjects 
			for (int i = 0; i < m_Path.Count; i++) {
				//dont draw anything on the currently selected unit
				if (m_Path[i] == UnitSelect.m_SelectedUnit.m_CurrTile) {
					continue;
				}
				GameObject go = new GameObject();
				SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
				sr.color = col;
				sr.sprite = m_WhiteSprite;
				sr.sortingOrder = 5;
				go.transform.position = m_Path[i].transform.position;
				go.transform.parent = transform;
			}




		}

	}

	//will move Selected unit to current place of mouse
	private void moveUnit() {
		Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		Tile mouseTile = Map.m_CurrentMap.getTileFromPosition(mousePos.x, mousePos.y);
		if (mouseTile == null) {
			return;
		}

		//if the tile the player clicked on is not included in list of tiles where unit can move then return
		if (!m_TilesNearBy.Contains(mouseTile)) {
			return;
		}

		//tell the class to attack after the movement is over
		if (m_AttackTile != null) {
			m_AttackAfterWalk = true;
		}

		//if there is a path
		if (m_Path != null) {
			//print(m_Path.Count);

			//send path to unit
			UnitSelect.m_SelectedUnit.setPath(Map.getDirectionFromPath(m_Path));


			//remove tiles from m_TilesNearBy list since the unit has now moved
			unitDeselected();

		}
	}

	//called by unityEvent system when a unit is selected
	//TODO check if friendly or enemy
	public void unitSelected() {
		m_CurrentMouseTile = null;

		List<Tile> tiles = null;

		tiles = Pathfind.getTilesInTilesArea(UnitSelect.m_SelectedUnit.m_CurrTile, UnitSelect.m_SelectedUnit.m_MovementRange);

		if (tiles != null) {

			for (int i = 0; i < tiles.Count; i++) {

				//if this tile has a unit on it then don't change the color
				if (tiles[i].m_UnitOnTile != null) {
					continue;
				}

				if (tiles[i].m_NextToEnemy) {
					tiles[i].GetComponent<SpriteRenderer>().color = new Color(1, 0.0f, 0.0f, 0.5f);
				} else {
					tiles[i].GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.2f);
				}
			}
			m_TilesNearBy = tiles;
		}
	}

	//called by unityEvent system when a unit is deselected
	public void unitDeselected() {
		for (int i = 0; i < transform.childCount; i++) {
			Destroy(transform.GetChild(i).gameObject);
		}
		if (m_TilesNearBy != null) {
			for (int i = 0; i < m_TilesNearBy.Count; i++) {
				m_TilesNearBy[i].GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
			}
		}
		m_TilesNearBy = null;
	}
}

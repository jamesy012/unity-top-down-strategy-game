﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SpriteRenderer))]
public class UnitSelect : MonoBehaviour {

	public static Unit m_SelectedUnit = null;

	private SpriteRenderer m_SelectSprite;

	public UnityEvent m_UnitSelected;
	public UnityEvent m_UnitDeselected;

	// Use this for initialization
	void Start() {
		m_SelectSprite = GetComponent<SpriteRenderer>();
		m_SelectSprite.enabled = false;
	}

	// Update is called once per frame
	void Update() {

		//only update position if there is a selected unit
		if (m_SelectedUnit != null) {
			transform.position = m_SelectedUnit.transform.position;
		}

		if (Unit.m_UnitMoving) {
			return;
		}

		if (BattleScreen.m_BattleScreen != null) {
			if (BattleScreen.m_BattleScreen.m_IsOpen) {
				if (m_SelectedUnit != null) {
					deSelect();
				}
				return;
			}
		}

		//if escape was pressed, then reselect unit
		if (Input.GetKeyDown(KeyCode.Escape)) {
			//if there is a selected unit
			if (m_SelectedUnit != null) {
				deSelect();
			}
		}

		//only run update if the leftmouse button was pressed
		if (Input.GetMouseButtonDown(0)) {

			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			int layer = 1 << LayerMask.NameToLayer("Units");

			Unit selected = null;

			if (Physics.Raycast(ray, out hit, Mathf.Abs(Camera.main.transform.position.z), layer)) {
				selected = hit.transform.GetComponent<Unit>();
			}

			//if there was a selected unit deselect it then select the new one
			if (selected != null) {
				//show sprite for selection
				m_SelectSprite.enabled = true;
				//if a unit is already selected then invoke for it to be deselected
				if (m_SelectedUnit != null) {
					m_UnitDeselected.Invoke();
				}
				//change m_SelectedUnit to the new selected unit
				m_SelectedUnit = selected;
				//invoke newly selected unit
				m_UnitSelected.Invoke();
			} else {//else just deselect
				deSelect();
			}

		}



	}

	private void deSelect() {

		//disable sprite for selection
		m_SelectSprite.enabled = false;
		//invoke current unit being deselected
		m_UnitDeselected.Invoke();
		//remove currently selected unit
		m_SelectedUnit = null;

	}
}
